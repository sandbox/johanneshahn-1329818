<?php

/**
 * @file
 * Module admin page callbacks.
 */

//////////////////////////////////////////////////////////////////////////////
// File antivirus settings

/**
 * Implements the settings page.
 *
 * @return
 *   The form structure.
 */
function ffw2_antivirus_admin_settings() {
  $form = array();

  $form['antivirus'] = array('#type' => 'fieldset', '#title' => t('ClamAV'), '#collapsible' => TRUE, '#collapsed' => FALSE);
  $form['antivirus']['ffw2_antivirus_clamav'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable <a href="http://www.clamav.net" title="ClamAV">ClamAV</a> anti-virus scans'),
    '#default_value' => FFW2_ANTIVIRUS_CLAMAV,
  );
  $form['antivirus']['ffw2_antivirus_clamav_method'] = array(
    '#type'          => 'radios',
    '#title'         => t('Select anti-virus scan method'),
    '#options'       => array(FALSE => t('Connect to a \'clamav-daemon\' TCP socket'), TRUE => t('Run a \'clamscan\' program')),
    '#default_value' => FFW2_ANTIVIRUS_CLAMAV_METHOD,
    '#description'   => t('A preferable way is to use a \'clamav-daemon\' since it ensures much faster performance then a \'clamscan\' program. Daemon should have a read access to the web server\'s temporal files in order to scan them.'),
  );
  $form['antivirus']['ffw2_antivirus_clamav_allow'] = array(
    '#type'          => 'radios',
    '#title'         => t('Allow file uploads if the daemon is not running or program is not found'),
    '#options'       => array(FALSE => t('No'), TRUE => t('Yes')),
    '#default_value' => FFW2_ANTIVIRUS_CLAMAV_ALLOW,
    '#description'   => t('Choose if you want to allow file uploads when scan is enabled, but the daemon is not running or program is not found.'),
  );
  // Deamon.
  $clamav_daemon_version = _ffw2_antivirus_check_clamav(array('host' => FFW2_ANTIVIRUS_CLAMAV_HOST, 'port' => FFW2_ANTIVIRUS_CLAMAV_PORT, 'version' => TRUE));
  $clamav_daemon_version = preg_match('/ClamAV/', $clamav_daemon_version) ? $clamav_daemon_version : NULL;
  $message = isset($clamav_daemon_version) ? t('The \'clamav-daemon\' %version is found at %host:%port.', array('%host' => FFW2_ANTIVIRUS_CLAMAV_HOST, '%port' => FFW2_ANTIVIRUS_CLAMAV_PORT, '%version' => $clamav_daemon_version)) : t('The clamav-daemon is not found at %host:%port.', array('%host' => FFW2_ANTIVIRUS_CLAMAV_HOST, '%port' => FFW2_ANTIVIRUS_CLAMAV_PORT));
  $form['daemon'] = array('#type' => 'fieldset', '#title' => t('ClamAV daemon'), '#description' => t('These settings will be used if you have chosen "Connect to a \'clamav-daemon\' TCP socket" above. '. $message), '#collapsible' => TRUE, '#collapsed' => FALSE);
  $form['daemon']['ffw2_antivirus_clamav_host'] = array(
    '#type'          => 'textfield',
    '#title'         => t('\'clamav-daemon\' host'),
    '#default_value' => FFW2_ANTIVIRUS_CLAMAV_HOST,
    '#maxlength'     => 255,
    '#description'   => t('A hostname \'clamav-daemon\' is running on. For most cases it will run on the same machine as a webserver.'),
  );
  $form['daemon']['ffw2_antivirus_clamav_port'] = array(
    '#type'          => 'textfield',
    '#title'         => t('\'clamav-daemon\' TCP port'),
    '#default_value' => FFW2_ANTIVIRUS_CLAMAV_PORT,
    '#size'          => 6,
    '#maxlength'     => 8,
    '#description'   => t('A TCP port \'clamav-daemon\' is listening to. Default port for the daemon is 3310.'),
  );
  // Utility.
  $form['utility'] = array('#type' => 'fieldset', '#title' => t('ClamAV program'), '#description' => t('These settings will be used if you have chosen "Run a \'clamscan\' program" above.'), '#collapsible' => TRUE, '#collapsed' => FALSE);
  if ($clamav_utility_path = _ffw2_command_exists('clamscan')) {
    $clamav_utility_version = _ffw2_antivirus_check_clamav(array('path' => $clamav_utility_path));
    $clamav_utility_version = preg_match('/ClamAV/', $clamav_utility_version) ? $clamav_utility_version : NULL;
  }
  $message = isset($clamav_utility_version) ? t('The \'clamscan\' %version is found at %path. If the field is left empty the default utility will be used.', array('%path' => $clamav_utility_path, '%version' => $clamav_utility_version)) : t('The \'clamscan\' is not found. Please, specify the full path.');
  $form['utility']['ffw2_antivirus_clamav_path'] = array(
    '#type'          => 'textfield',
    '#title'         => t('A path to the scanning utility \'clamscan\''),
    '#default_value' => FFW2_ANTIVIRUS_CLAMAV_PATH,
    '#maxlength'     => 255,
    '#description'   => t('Full path to the \'clamscan\' utility.') .' '. $message,
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['reset'] = array(
    '#type'  => 'submit',
    '#value' => t('Reset to defaults'),
  );

  return $form;
}

/**
 * Validate hook for the settings form.
 */
function ffw2_antivirus_admin_settings_validate($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  $values = $form_state['values'];
  switch ($op) {
    case t('Save configuration'):
      $clamav_path = $values['ffw2_antivirus_clamav_path'];
      if ($values['ffw2_antivirus_clamav_method'] && !empty($clamav_path)) {
        if (!preg_match('/clamscan$/', $clamav_path)) {
          form_set_error('ffw2_antivirus_clamav_path', t('Invalid path %path specified for the \'clamscan\' utility. The path should end with a \'clamscan\'.', array('%path' => $clamav_path)));
        }
        elseif (!file_exists($clamav_path)) {
          form_set_error('ffw2_antivirus_clamav_path', t('Invalid path %path specified for the \'clamscan\' utility. Program not found.', array('%path' => $clamav_path)));
        }
        elseif (!preg_match('/ClamAV/', _ffw2_antivirus_check_clamav(array('path' => $clamav_path)))) {
          form_set_error('ffw2_antivirus_clamav_path', t('Invalid path %path specified for the \'clamscan\' utility. Utility have not returned it\'s version string.', array('%path' => $clamav_path)));
        }
      }
      if (!$values['ffw2_antivirus_clamav_method']) {
        if (empty($values['ffw2_antivirus_clamav_host'])) {
          form_set_error('ffw2_antivirus_clamav_host', t('A hostname should be provided to connect to.'));
        }
        if (empty($values['ffw2_antivirus_clamav_port'])) {
          form_set_error('ffw2_antivirus_clamav_port', t('A port should be provided to connect to.'));
        }
        elseif (!ctype_digit($values['ffw2_antivirus_clamav_port'])) {
          form_set_error('ffw2_antivirus_clamav_port', t('A port should only contain digits.'));
        }
      }
      break;
  }
}

/**
 * Submit hook for the settings form.
 */
function ffw2_antivirus_admin_settings_submit($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  $values = $form_state['values'];
  switch ($op) {
    case t('Save configuration'):
      variable_set('ffw2_antivirus_clamav', $values['ffw2_antivirus_clamav']);
      variable_set('ffw2_antivirus_clamav_method', $values['ffw2_antivirus_clamav_method']);
      variable_set('ffw2_antivirus_clamav_allow', $values['ffw2_antivirus_clamav_allow']);
      variable_set('ffw2_antivirus_clamav_host', $values['ffw2_antivirus_clamav_host']);
      variable_set('ffw2_antivirus_clamav_port', $values['ffw2_antivirus_clamav_port']);
      variable_set('ffw2_antivirus_clamav_path', $values['ffw2_antivirus_clamav_path']);
      drupal_set_message(t('The configuration options have been saved.'));
      break;
    case t('Reset to defaults'):
      variable_del('ffw2_antivirus_clamav');
      variable_del('ffw2_antivirus_clamav_method');
      variable_del('ffw2_antivirus_clamav_allow');
      variable_del('ffw2_antivirus_clamav_host');
      variable_del('ffw2_antivirus_clamav_port');
      variable_del('ffw2_antivirus_clamav_path');
      drupal_set_message(t('The configuration options have been reset to their default values.'));
      break;
  }
}

