
/**
 * Conditionally show or hide settings.
 */
Drupal.behaviors.ffw2_attach_admin = function () {
  $('div.ffw2-attach-attachments-ffw2-attach-reuse-setting input.form-checkbox').click(function () {
    if (this.checked) {
      $('div.ffw2-attach-attachments-ffw2-attach-reuse-nodes-setting').show();
    }
    else {
      $('div.ffw2-attach-attachments-ffw2-attach-reuse-nodes-setting').hide();
    }
  });
  $('div.ffw2-attach-vocabs-ffw2-attach-vocabularies-all-setting input.form-radio').click(function () {
    if (this.value == '0') {
      $('div.ffw2-attach-vocabs-ffw2-attach-vocabularies-setting').show();
    }
    else {
      $('div.ffw2-attach-vocabs-ffw2-attach-vocabularies-setting').hide();
    }
  });
};

