
/**
 * Setting initial variables.
 */
Drupal.ffw2_browserVars = {
  'selectedID':null, // selected object id
  'pageX':0, // where the mouseX pointer is
  'pageY':0, // where the mouseY pointer is
  'dropDownTimeoutId':0, // timeout id for hiding action div
  'filecount':1, // current number of file inputs
  'messageTimeout':5000, // timeout to show drupal mesages
  'progressTimeoutId':0, // id of the progress bar timeout
  'animationTimeout':1000 // fadeIn, fadeOut time
};

/**
 * Behaviours are bound to the Drupal namespace.
 */
Drupal.behaviors.ffw2_browser = function(context) {  
  $('.ffw2-dropdown').mouseout(function() {
    Drupal.ffw2_browserVars.dropDownTimeoutId = setTimeout(function() { $('.ffw2-dropdown').hide(); }, 1000); // hide action div
  }).mouseover(function() {
    clearTimeout(Drupal.ffw2_browserVars.dropDownTimeoutId); // remove timeout for hiding the action div
  });

  $(document).mousemove(function(e) {
    // msie does not support pageX so handle that incase needed
    Drupal.ffw2_browserVars.pageX = e.pageX ? e.pageX : e.clientX;
    Drupal.ffw2_browserVars.pageY = e.pageY ? e.pageY : e.clientY;
    if ($.browser['msie'] == true && $.browser.version < 7.0) { Drupal.ffw2_browserVars.pageY += 10; };
  });

  $('#edit-upload-0').change(function() {
    Drupal.ffw2_browserCheckAddFileWidget();
  });
};
 
/**
 * Set the height of the file system holder based off the window height.
 */
Drupal.ffw2_browserInit = function(block) {
  var ratio = block == 'page' ? 0.75 : 0.3;
  $('div.ffw2-system').height('' + (Drupal.ffw2_browserGetWindowHeight() * ratio) + 'px');
  var settings = Drupal.settings.ffw2_browser;
  $('#ffw2-preview').show().html(settings.no_ffw2_selected);
};

/**
 * Gets window height.
 */
Drupal.ffw2_browserGetWindowHeight = function() {
  if (typeof(window.innerHeight) == 'number')
    return window.innerHeight; // !MSIE
  if (document.documentElement && document.documentElement.clientHeight)
    return document.documentElement.clientHeight; // MSIE6
  if (document.body && document.body.clientHeight)
    return document.body.clientHeight; // MSIE4
  return 600; // reasonable default
};

/**
 * Set the vocabulary and term IDs on the folder.
 */
Drupal.ffw2_browserSetContext = function(id, parentId) {
  var settings = Drupal.settings.ffw2_browser;
  var folderId = id.match(/ffw2-folder/) ? id : parentId;
  var isTerm = id.match(/-([vt])[\d]+/)[1] == 't' ? true : false;
  var tid = isTerm ? id.match(/-[vt]([\d]+)/)[1] : 0;

  // setting the term id that the file will be uploaded too
  $('#ffw2-upload-tid').val(tid);
  // retrieve vid from the file system for use in the create term form
  var filesysvid = $('#' + folderId).parents().find('.ffw2-system').attr('id');
  var vid = filesysvid.match(/ffw2-system-([\d]+)/);
  // if vid not in the file system folder get it from the file folder
  if (!vid) {
    while (!id.match(/-v([\d]+)/))
      id = $('#' + id).parent().attr('id');
    vid = id.match(/-v([\d]+)/)[1];
  }
  // setting the vocabulary id that the term will be created under
  $('#newterm-vid').val(vid);
  // setting the term id the file will be created under
  $('#newterm-parent').val(tid);
  // toggle the file upload button
  var button = $('input#edit-upload-submit');
  isTerm && $('#ffw2-upload-progress').is(':hidden') ? button.removeAttr('disabled') : button.attr('disabled', 'disabled');
  // toggle the new term creation button
  button = $('input#edit-newterm-submit');
  $('#' + folderId).hasClass('hierarchy') ? button.removeAttr('disabled') : button.attr('disabled', 'disabled');
  // select and disable current group checkbox
  $('#ffw2-browser-upload-form').find('.form-checkbox').each(function() { this.checked = false; }).removeAttr('disabled');
  if (folderId.match(/-g([\d]+)/)) {
    var gid = id.match(/-g([\d]+)/)[1];
    $('#edit-og-groups-' + gid).each(function() { this.checked = true; }).attr('disabled', 'disabled');
  }
  $('#ffw2-preview-spinner').hide();
  $('#ffw2-preview').show().html(settings.no_file_selected)
};

/**
 * Dynamically add the new Term to the DOM in the correct location
 * @param block {String} browser id
 * @param tid {Integer} term which we add
 * @param ptid {Integer} parent term to which we are appending the new node under
 * @param vid {Integer} if parent term is 0 then insert node under the correct vocabulary
 * @param gid {Integer} a og_vocab group id or 0
 * @param node {String} html representation of the node
 * @param msg {String} message to display on screen to the user
 * @param nodename {String} name of the new node being created
 */
Drupal.ffw2_browserDisplayTerm = function(block, tid, ptid, vid, gid, node, msg, nodename) {
  var folder = 'ffw2-folder-' + (ptid == 0 ? 'v' + vid : 't' + ptid) + '-g' + gid + '-b' + block;
  var term = 'ffw2-folder-t' + tid + '-g' + gid + '-b' + block;
  var check = 0;
  var child = 0;
  $('#' + folder).children().each(function() {
    if (this.id.match(/ffw2-folder-/)) {
      var name = $(this).find('.title').text();
      if (name.toLowerCase() > nodename.toLowerCase() && check == 0) {
        check = 1;
        $(node).insertBefore($(this));
      };
      child = 1;
    };
  });
  // the node might be the last one or there is no child nodes
  if (check == 0) {
    // if there were no child nodes
    if (child == 0)
      $('#' + folder + ' div.ffw2-cells:first').after(node);
    else
      $('#' + folder + ' div.ffw2-folder:last').after(node);
  };
  // remove empty class
  if ($('#' + folder).hasClass('empty')) {
    $('#' + folder).removeClass('empty')
  }
  // remove the term if the folder is not expanded
  if (!$('#' + folder).is('.expanded')) {
    $('#' + term).remove();
    // now expand the parent shelf
    $('#' + folder + ' div.ffw2-cells:first').each(function() { Drupal.ffw2_browserFolderClick(this, term) });
  }
  // display the term
  $('#' + term).fadeIn(Drupal.ffw2_browserVars.animationTimeout);
  setTimeout('$(\'#' + term + '\').css(\'display\', \'block\')', Drupal.ffw2_browserVars.animationTimeout);
  // select the newly created folder
  $('#' + term + ' div.ffw2-cells:first').each(function() { Drupal.ffw2_browserFolderClick(this) });
  Drupal.ffw2_browserSetMsg(msg);
  $('#ffw2-browser-newterm-form input#newterm-name').val(''); // resetting the value in the text field
};

/**
 * Dynamically add the new node to the DOM in the correct location
 * @param block {String} browser id
 * @param nid {Integer} term which we add
 * @param tid {Integer} parent term where the new node is being inserted into
 * @param node {String} html representation of the node
 * @param msg {String} message to display on screen to the user
 * @param nodename {String} name of the new node being created
 * @param last {Boolean} true if several nodes are added in bulk and this node is the last one
 */
Drupal.ffw2_browserDisplayNode = function(block, nid, ptid, gid, node, msg, nodename, last) {
  var folder = 'ffw2-folder-t' + ptid + '-g' + gid + '-b' + block;
  var file = 'ffw2-node-t' + ptid + '-n' + nid + '-b' + block;
  var check = 0;
  $('#' + folder).children().each(function() {
    if (this.id.match(/ffw2-node-/)) {
      var name = $(this).find('.title').text();
      if (name.toLowerCase() > nodename.toLowerCase() && check == 0) {
        check = 1;
        $(node).insertBefore($(this));
      };
    };
  });
  // the node might be the last one or there is no child nodes
  if (check == 0) {
      $('#' + folder).append(node);
  };
  // remove empty class
  if ($('#' + folder).hasClass('empty')) {
    $('#' + folder).removeClass('empty')
  }
  // hide the term if the folder is not expanded
  if (!$('#' + folder).is('.expanded')) {
    $('#' + file).remove();
    // now expand the parent shelf
    if (last) {
      $('#' + folder + ' div.ffw2-cells:first').each(function() { Drupal.ffw2_browserFolderClick(this, file) });
    }
  }
  else {
    // display the term
    $('#' + file).fadeIn(Drupal.ffw2_browserVars.animationTimeout);
    setTimeout('$(\'#' + file + '\').css(\'display\', \'block\')', Drupal.ffw2_browserVars.animationTimeout);
  }
  if (last) {
    // select the newly created file
    $('#' + file + ' div.ffw2-cells:first').each(function() { Drupal.ffw2_browserFileClick(this) });
    // resetting the form for file upload back to its original state
    if ($('#ffw2_browser').find('input:file').size() > 1) {
      $('#ffw2_browser').find('input:file').each(function() {
        if (this.id != 'edit-upload') {
          $(this).parent().remove(); // remove it from the DOM we do not need it, parent is the <p>
        }
      });
    }
  }
  Drupal.ffw2_browserSetMsg(msg);
};

/**
 * Adds informational messages to the screen for the user to see
 * @param msg {String} message to be displayed on the screen to the user
 */
Drupal.ffw2_browserSetMsg = function(msg) {
  if (!$('#ffw2-upload-messages-container').size())
    $('<div id="ffw2-upload-messages-container"><div id="ffw2-upload-messages" class="messages status"></div></div>').insertBefore('.ffw2-system');
  $('#ffw2-upload-messages').append(msg);
  setTimeout(function() { $('#ffw2-upload-messages-container').remove(); }, Drupal.ffw2_browserVars.messageTimeout); // clear the messages after period of time
};

/**
 * Display any error messages on the screen since we do not do full page refreshes for actions
 * @param msg {String} error message to be displayed to the user
 */
Drupal.ffw2_browserErrorMsg = function(msg) {
  if (!$('#ffw2-upload-errors-container').size())
    $('<div id="ffw2-upload-errors-container"><div id="ffw2-upload-errors" class="messages error"></div></div>').insertBefore('.ffw2-system');
  $('#ffw2-upload-errors').append(msg);
  setTimeout(function() { $('#ffw2-upload-errors-container').remove(); }, Drupal.ffw2_browserVars.messageTimeout); // clear the messages after period of time
};

/**
 * Display any status messages on the screen since we do not do full page refreshes for actions
 * @param msg {String} status message to be displayed to the user
 */
Drupal.ffw2_browserStatusMsg = function(msg) {
  if (!$('#ffw2-upload-statuses-container').size())
    $('<div id="ffw2-upload-statuses-container"><div id="ffw2-upload-statuses" class="messages status"></div></div>').insertBefore('.ffw2-system');
  $('#ffw2-upload-statuses').append(msg);
  setTimeout(function() { $('#ffw2-upload-statuses-container').remove(); }, Drupal.ffw2_browserVars.messageTimeout); // clear the messages after period of time
};

/**
 * Display any notice messages on the screen since we do not do full page refreshes for actions
 * @param msg {String} status message to be displayed to the user
 */
Drupal.ffw2_browserNoticeMsg = function(msg) {
  if (!$('#ffw2-upload-notices-container').size())
    $('<div id="ffw2-upload-notices-container"><div id="ffw2-upload-notices" class="messages notice"></div></div>').insertBefore('.ffw2-system');
  $('#ffw2-upload-notices').append(msg);
  setTimeout(function() { $('#ffw2-upload-notices-container').remove(); }, Drupal.ffw2_browserVars.messageTimeout); // clear the messages after period of time
};

/**
 * Highlights the selected row and adds necessary css classes to the pertinent rows
 * @param id {String} Parent id of the selected DOM object
 * @param folder {Boolean} Is it a folder {true = folder, false = not a folder}
 * @param toggle {Boolean} If true, 'expanded' class will be toggled
 */
Drupal.ffw2_browserSelectRow = function(id, folder, toggle) {
  if (id != Drupal.ffw2_browserVars.selectedId)
    if (Drupal.ffw2_browserVars.selectedId && ($('#' + Drupal.ffw2_browserVars.selectedId)))
      $('#' + Drupal.ffw2_browserVars.selectedId).children('.ffw2-cells').removeClass('selected');
  Drupal.ffw2_browserVars.selectedId = id;
  $('#' + id).children('.ffw2-cells').addClass('selected');

  if (folder && toggle)
    $('#' + id).toggleClass('expanded');
};

/*
 * Expands or collapses the folder that was selected based on the current DOM information
 * @param obj {Object} The folder object that was clicked on
 * @param elm {String} The id of the element to click on the folder expand
 */
Drupal.ffw2_browserFolderClick = function(obj, elm) {
  var settings = Drupal.settings.ffw2_browser;
  var parent = obj.parentNode;
  var id = parent.id;
  var block = id.match(/-b([^-]+)/)[1];
  var parentId = null;
  if (id.match(/-t([\d]+)/)) {
    tid = 'term/' + id.match(/-t([\d]+)/)[1];
    parentId = obj.parentNode.parentNode.id;
  }
  else {
    tid = 'voc/' + id.match(/-v([\d]+)/)[1];
  }
  // highlighting the row that was selected
  Drupal.ffw2_browserSelectRow(id, true, true);
  // hiding the block
  $('#ffw2-preview').hide();
  // upload and newterm are only on the main page
  if (block == 'page') {
    Drupal.ffw2_browserSetContext(id, parentId);
  }
  // if the shelf currently is closed then open the shelf
  // the class 'expanded' is already toggled in SelectRow.
  if ($(parent).hasClass('expanded')) {
    if (!$(parent).hasClass('empty')) {
      $('#' + id + '-spinner').show();
    }
    $.get(settings.ajax_url + '/' + tid + '/' + block, function(result) {
      $(obj).parent().addClass('expanded');
      // to stop duplicate info from double clicks
      if (parent.childNodes.length == 1) {
        $('#' + id).append(result);
        $('#' + id).find('span.ffw2.with-menu').click(function(event) {
          $(this).toggleClass('active');
          $(this).find('ul').toggle();
        });
        $('a.ffw2-metadata').cluetip({arrows: true});
      }
      $('#' + id + '-spinner').hide();
      if (typeof(elm) != 'undefined') {
        // click the element
        if (elm.match(/ffw2-folder/))
          $('#' + elm + ' div.ffw2-cells:first').each(function() { Drupal.ffw2_browserFolderClick(this) });
        else
          $('#' + elm + ' div.ffw2-cells:first').each(function() { Drupal.ffw2_browserFileClick(this) });
      }
    });
  } else {
    // shelf is open hence remove all the children except the shelf name
    $(obj.parentNode).children().each(function() {
      if (this.nodeName.toLowerCase() == 'div' && this.id)
        parent.removeChild(this);
      else if (this.nodeName.toLowerCase() == 'script')
        parent.removeChild(this);
    });
  }
  return false;
};

/**
 * Handle operations when one of the file rows is clicked
 * @param obj {Object} File Object that holds a file which was clicked
 */
Drupal.ffw2_browserFileClick = function(obj) {
  var id = obj.parentNode.id;
  var nid = id.match(/-n([\d]+)/)[1]; // node id
  var block = id.match(/-b([^-]+)/)[1];
  var tid = id.match(/-t([\d]+)/)[1]; // term id
  var parentId = obj.parentNode.parentNode.id;
  // highlighting the row that was selected
  Drupal.ffw2_browserSelectRow(id, false, true);
  // show preview
  if (block == 'page') {
    Drupal.ffw2_browserSetContext(id, parentId);
    $('#ffw2-preview').hide();
    $('#ffw2-preview-spinner').show();
    $.get($('#ffw2-preview-url').val() + '/' + nid + '/' + tid, function(result) {
      $('#ffw2-preview-spinner').hide();
      $('#ffw2-preview').html(result).show();
      if (typeof collapseAutoAttach != 'undefined') {
        collapseAutoAttach();
      };
    });
  }
  return false;
};

/**
 * Adds upload widget.
 */
Drupal.ffw2_browserAddFileWidget = function() {
  var settings = Drupal.settings.ffw2_browser;
  // creating the new file widget that will be added to the form
  var widget = document.createElement("input");
  widget.setAttribute("type", "file");
  widget.setAttribute("name", "files[upload_" + Drupal.ffw2_browserVars.filecount + "]");
  widget.setAttribute("id", "edit-upload-" + Drupal.ffw2_browserVars.filecount);
  widget.setAttribute("class", "form-ffw2");
  widget.setAttribute("size", "10");
  // creating widget wrapper
  var wrapper = document.createElement("div");
  wrapper.setAttribute('id', 'edit-upload-' + Drupal.ffw2_browserVars.filecount + '-wrapper');
  wrapper.setAttribute('class', 'form-item');
  wrapper.appendChild(widget);
  var div = document.createElement("div");
  div.appendChild(wrapper);
  // adding the new widget to the container widget
  $('#ffw2-upload').find('input:file').each(function() {
    if (this.id == 'edit-upload-' + (Drupal.ffw2_browserVars.filecount - 1)) {
      $(div).insertAfter($(this).parent().parent());
    }
  });
  // binding on change function
  $('#edit-upload-' + Drupal.ffw2_browserVars.filecount).change(function() {
    Drupal.ffw2_browserCheckAddFileWidget();
  });
  // integration with file restriction module
  if (typeof(Drupal.ffw2_restrictionExtensions) == 'function') {
    Drupal.ffw2_restrictionExtensions('#edit-upload-' + Drupal.ffw2_browserVars.filecount);
  }
  Drupal.ffw2_browserVars.filecount++;
  if (Drupal.ffw2_browserVars.filecount == settings.max_upload) {
    $('#ffw2-browser-add-ffw2-link').hide();
  }
};

/**
 * Checks if add upload widget is needed.
 */
Drupal.ffw2_browserCheckAddFileWidget = function() {
  var settings = Drupal.settings.ffw2_browser;
  var empty = false;
  for (var i=0;i<Drupal.ffw2_browserVars.ffw2count;i++) {
    if ($('#edit-upload-' + i).val() == '') {
      empty = true;
    }
  }
  if (!empty && Drupal.ffw2_browserVars.filecount < settings.max_upload) {
    Drupal.ffw2_browserAddFileWidget();
  }
}

/**
 * Removes upload widgets.
 */
Drupal.ffw2_browserDelFileWidget = function() {
  for (var i=1;i<Drupal.ffw2_browserVars.filecount;i++) {
    $('#edit-upload-' + i).parent().remove();
  }
  Drupal.ffw2_browserVars.filecount = 1;
  $('#ffw2-browser-upload-form input#edit-upload-0').val(''); // resetting the value in the text field
  $('.ffw2-restriction').hide();
  $('#ffw2-upload-progress').hide();
  var settings = Drupal.settings.ffw2_browser;
  $('#ffw2-browser-add-ffw2-link').show();
  if (settings.apc) {
    $('#edit-progress-key').val(settings.progress_id); // reset the APC id
    $('div.filled').css('width', '0%');
    $('div.percentage').html('');
    clearTimeout(Drupal.ffw2_browserVars.progressTimeoutId);
  }
};

/**
 * Sets parent term values on the ffw2 upload.
 */
Drupal.ffw2_browserUpdateTerm = function(id, size, files) {
  $('#' + id).children().each(function() {
    if (this.id == '') {
      $(this).find('.file-size').text(size);
      $(this).find('.file-date').text(files);
    }
  })
};

/**
 * Handle operations when file upload is clicked
 * @param obj {Object} File Upload Object <a> tag
 */
Drupal.ffw2_browserFileUploadClick = function(obj) {
  setTimeout(function() { $('input#edit-upload-submit').attr('disabled', 'disabled'); }, 100);
  var settings = Drupal.settings.ffw2_browser;
  $('div.filled').css('width', '0%');
  $('div.percentage').html('');
  $('div.message').html('');
  $('#ffw2-upload-progress').show();
  if (settings.apc) {
    Drupal.ffw2_browserVars.progressTimeoutId = setTimeout('Drupal.ffw2_browserGetProgress(' + settings.progress_timeout + ')', settings.progress_interval);
  }
  return true;
};

/**
 * Get the progress bar
 * @param timeout {Integer} If the APC is installed but not configured,
 *   the script will make timout times connections untill it times out.
 */
Drupal.ffw2_browserGetProgress = function(timeout) {
  var settings = Drupal.settings.ffw2_browser;
  $.getJSON(settings.progress_url, function(progress) {
    if (progress.percentage >= 0 && progress.percentage <= 100) {
      $('div.filled').css('width', progress.percentage + '%');
      $('div.percentage').html(progress.percentage + '%');
    }
    $('div.message').html(progress.message);
    if (progress.percentage < 100) {
      if (progress.percentage >= 0 || (progress.percentage == -1 && timeout > 1)) {
        Drupal.ffw2_browserVars.progressTimeoutId = setTimeout('Drupal.ffw2_browserGetProgress(' + (timeout-1) + ')', settings.progress_interval);
      }
    }
  });
};

