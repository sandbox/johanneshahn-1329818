
/**
 * Conditionally show or hide settings.
 */
Drupal.behaviors.ffw2_browser_admin = function () {
  $('div.ffw2-browser-vocabs-ffw2-browser-vocabularies-all-setting input.form-radio').click(function () {
    if (this.value == '0') {
      $('div.ffw2-browser-vocabs-ffw2-browser-vocabularies-setting').show();
    }
    else {
      $('div.ffw2-browser-vocabs-ffw2-browser-vocabularies-setting').hide();
    }
  });
};

