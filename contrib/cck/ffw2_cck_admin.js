
/**
 * Conditionally show or hide settings.
 */
Drupal.behaviors.ffw2_cck_admin = function () {
  $('div.ffw2-cck-vocabs-ffw2-cck-vocabularies-all-setting input.form-radio').click(function () {
    if (this.value == '0') {
      $('div.ffw2-cck-vocabs-ffw2-cck-vocabularies-setting').show();
    }
    else {
      $('div.ffw2-cck-vocabs-ffw2-cck-vocabularies-setting').hide();
    }
  });
};

