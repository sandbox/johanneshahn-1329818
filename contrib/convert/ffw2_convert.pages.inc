<?php

/**
 * @file
 * User page callbacks.
 */

//////////////////////////////////////////////////////////////////////////////
// File conversion

/**
 * Re-run file conversions for a file.
 *
 * @param $node
 *   A node object as returned from node_load().
 * @param $vid
 *   A node version ID.
 */
function ffw2_convert_rerun($node, $vid) {
  if (isset($vid) && $node->vid != $vid) {
    $node = node_load($node->nid, $vid);
  }
  if (!user_access('re-run ffw2 conversions') || $node->type != 'ffw2' || !is_object($node->ffw2) || (!empty($vid) && !is_numeric($vid))) {
    drupal_not_found();
    exit;
  }
  $file = $node->ffw2;
  $file->filename = $node->title;
  $file->filemime = $file->type;
  unset($file->type);
  $file->force_delete = TRUE;
  $file->keep_blob = TRUE;
  $file->keep_rdf = TRUE;
  ffw2_node_delete_node($file);
  ffw2_generate_previews($file, TRUE);
  drupal_goto('node/'. $node->nid);
}

