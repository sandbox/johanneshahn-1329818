
/**
 * Conditionally show or hide settings.
 */
Drupal.behaviors.ffw2_convert_admin = function () {
  $('div.ffw2-convert-performance-ffw2-convert-restrictions-enabled-setting input.form-checkbox').click(function () {
    if (this.checked) {
      $('div.ffw2-convert-performance-ffw2-convert-processes-commandline-setting').show();
      $('div.ffw2-convert-performance-ffw2-convert-restrictions-setting').show();
    }
    else {
      $('div.ffw2-convert-performance-ffw2-convert-processes-commandline-setting').hide();
      $('div.ffw2-convert-performance-ffw2-convert-restrictions-setting').hide();
    }
  });
};

