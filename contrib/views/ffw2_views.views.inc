<?php

/**
 * @file
 * Integrates file operations with the Views module.
 */

//////////////////////////////////////////////////////////////////////////////
// Views API hooks

/**
 * Implementation of hook_views_handlers().
 */
function ffw2_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'ffw2_views') .'/includes',
    ),
    'handlers' => array(
      'ffw2_views_handler_field_ffw2_nodes_numeric' => array(
        'parent' => 'views_handler_field',
      ),
      'ffw2_views_handler_field_ffw2_nodes_size' => array(
        'parent' => 'views_handler_field',
      ),
      'ffw2_views_handler_field_ffw2_nodes_type' => array(
        'parent' => 'views_handler_field',
      ),
      'ffw2_views_handler_field_ffw2_nodes_uri' => array(
        'parent' => 'views_handler_field',
      ),
      'ffw2_views_handler_field_ffw2_nodes_download' => array(
        'parent' => 'views_handler_field_node_link',
      ),
      'ffw2_views_handler_field_ffw2_nodes_view' => array(
        'parent' => 'views_handler_field_node_link',
      ),
      'ffw2_views_handler_field_ffw2_nodes_file' => array(
        'parent' => 'views_handler_field',
      ),
      'ffw2_views_handler_field_ffw2_attachments_fnid' => array(
        'parent' => 'views_handler_field',
      ),
      'ffw2_views_handler_field_ffw2_attachments_list' => array(
        'parent' => 'views_handler_field_prerender_list',
      ),
      'ffw2_views_handler_field_ffw2_attachments_count' => array(
        'parent' => 'views_handler_field',
      ),
      'ffw2_views_handler_field_ffw2_attachments_parents' => array(
        'parent' => 'views_handler_field_prerender_list',
      ),
      'ffw2_views_handler_filter_node_attachments' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 */
function ffw2_views_views_data() {
  $data = array();

  // ----------------------------------------------------------------------
  // ffw2_nodes table

  $data['ffw2_nodes']['table']['group']  = t('Files (ffw2)');
  $data['ffw2_nodes']['table']['join'] = array(
    'node' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
  );

  $data['ffw2_nodes']['size'] = array(
    'title' => t('Size'),
    'help' => t('The size of the file.'),
    'field' => array(
      'handler' => 'ffw2_views_handler_field_ffw2_nodes_size',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['ffw2_nodes']['type'] = array(
    'title' => t('Type'),
    'help' => t('The MIME type of the file.'),
    'field' => array(
      'handler' => 'ffw2_views_handler_field_ffw2_nodes_type',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['ffw2_nodes']['views'] = array(
    'title' => t('Viewed'),
    'help' => t('Display the number of times the file has been viewed.'),
    'field' => array(
      'handler' => 'ffw2_views_handler_field_ffw2_nodes_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['ffw2_nodes']['downloads'] = array(
    'title' => t('Downloaded'),
    'help' => t('Display the number of times the file has been downloaded.'),
    'field' => array(
      'handler' => 'ffw2_views_handler_field_ffw2_nodes_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['ffw2_nodes']['uri'] = array(
    'field' => array(
      'title' => t('URI'),
      'help' => t('Provide a file URI.'),
      'handler' => 'ffw2_views_handler_field_ffw2_nodes_uri',
    ),
  );

  $data['ffw2_nodes']['download'] = array(
    'field' => array(
      'title' => t('Download link'),
      'help' => t('Provide a simple link to download file.'),
      'handler' => 'ffw2_views_handler_field_ffw2_nodes_download',
    ),
  );

  $data['ffw2_nodes']['view'] = array(
    'field' => array(
      'title' => t('View link'),
      'help' => t('Provide a simple link to view file.'),
      'handler' => 'ffw2_views_handler_field_ffw2_nodes_view',
    ),
  );

  $data['ffw2_nodes']['ffw2'] = array(
    'field' => array(
      'title' => t('File'),
      'help' => t('Provide a file.'),
      'handler' => 'ffw2_views_handler_field_ffw2_nodes_file',
    ),
  );

  // ----------------------------------------------------------------------
  // file_attachments table

  $data['ffw2_attachments']['table']['group']  = t('Files');
  $data['ffw2_attachments']['table']['join'] = array(
    'node' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
  );

  $data['ffw2_attachments']['fnid'] = array(
    'title' => t('Attached file'),
    'help' => t('A file attached to a node.'),
    'field' => array(
      'handler' => 'ffw2_views_handler_field_ffw2_attachments_fnid',
    ),
    'relationship' => array(
      'title' => t('Node has attached files'),
      'help' => t('Add a relationship to show only nodes which have attachments.'),
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node has attachments'),
    ),
  );

  $data['ffw2_attachments']['list'] = array(
    'field' => array(
      'title' => t('All attached files'),
      'help' => t('A list of all files attached to a node.'),
      'handler' => 'ffw2_views_handler_field_ffw2_attachments_list',
    ),
  );

  $data['ffw2_attachments']['count'] = array(
    'field' => array(
      'title' => t('Number of attached files'),
      'help' => t('A number of the files attached to a node.'),
      'handler' => 'ffw2_views_handler_field_ffw2_attachments_count',
    ),
  );

  $data['ffw2_attachments']['parents'] = array(
    'field' => array(
      'title' => t('Attached to'),
      'help' => t('A list of all nodes the file is attached to.'),
      'handler' => 'ffw2_views_handler_field_ffw2_attachments_parents',
    ),
  );

  // ----------------------------------------------------------------------
  // nodetable

  $data['node']['attachments'] = array(
    'filter' => array(
      'title' => t('File nodes'),
      'help' => t('Whether file nodes are attachments.'),
      'handler' => 'ffw2_views_handler_filter_node_attachments'
    ),
  );

  return $data;
}

