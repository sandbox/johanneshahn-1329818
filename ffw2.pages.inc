<?php

/**
 * @file
 * User page callbacks.
 */

//////////////////////////////////////////////////////////////////////////////
// File metadata tab

/**
 * Outputs a themed metadata HTML page.
 *
 * @param $node
 *   A node object.
 *
 * @return
 *   Themed HTML metadata table.
 */
function ffw2_page_metadata($node) {
  $namespaces = rdf_get_namespaces();
  $header = array(
    array('data' => t('Name'), 'field' => 'name'),
    array('data' => t('Value'), 'field' => 'value'),
  );

  db_query("CREATE TEMPORARY TABLE ffw2_properties (name VARCHAR(255), value VARCHAR(255))");

  $info = ffw2_get_metadata_info();
  $data = rdf_normalize(rdf_query($node->ffw2->uri, NULL, NULL, array('repository' => FFW2_RDF_REPOSITORY)));
  foreach ($data as $subject => $predicates) {
    foreach ($predicates as $predicate => $objects) {
      foreach ($objects as $object) {
        $rdf = rdf_uri_to_qname($predicate, $namespaces, FALSE);
        if (isset($info[$rdf]) && ($name = $info[$rdf]['name'])) {
          db_query("INSERT INTO ffw2_properties (name, value) VALUES ('%s', '%s')", $name, isset($info[$rdf]['theme']) ? theme($info[$rdf]['theme'], is_object($object) ? $object->value : $object) : theme('rdf_value', $object));
        }
      }
    }
  }

  $rows = array();
  $result = db_query('SELECT * FROM ffw2_properties'. tablesort_sql($header));
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      //array('data' => $row->name .':', 'align' => 'right'),
      $row->name,
      $row->value,
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No data.'), 'colspan' => 2));
  }

  return theme('table', $header, $rows, array('class' => 'ffw2-properties'));
}

//////////////////////////////////////////////////////////////////////////////
// File formats tab

/**
 * Outputs a themed formats HTML page.
 *
 * @param $node
 *   A node object.
 *
 * @return
 *   Themed HTML formats table.
 */
function ffw2_page_formats($node) {
  $header = array(
    t('Format'),
    t('MIME type'),
    t('Size'),
    t('Status'),
  );

  $handlers = ffw2_get_mime_handlers();
  foreach (ffw2_generated_for($node->ffw2) as $uri => $data) {
    if ($node->ffw2->uri != $uri) {
      $description = preg_match('/^ffw2_image_/', $data['handler']) ? $handlers[$data['handler']]['name'] : NULL;
      $rows[] = array('data' => array(
        theme('ffw2_render', (object)array('nid' => $node->nid, 'vid' => $node->vid, 'name' => ffw2_mime_description_for($data['type']), 'type' => $data['type'], 'size' => $data['size'], 'uri' => $uri, 'derived' => TRUE, 'desc' => $description)),
        $data['type'],
        format_size($data['size']),
        t('OK'),
      ), 'class' => 'ffw2-formats-ok');
    }
  }
  foreach (ffw2_pending_previews($node->ffw2) as $pending) {
    list($mime, $handler) = explode(':', $pending);
    $rows[] = array('data' => array(
      ffw2_mime_icon_for($mime) .' '. ffw2_mime_description_for($mime),
      $mime,
      '-',
      t('Pending'),
    ), 'class' => 'ffw2-formats-pending');
  }
  foreach (ffw2_failed_previews($node->ffw2) as $failed) {
    list($mime, $handler) = explode(':', $failed);
    $rows[] = array('data' => array(
      ffw2_mime_icon_for($mime) .' '. ffw2_mime_description_for($mime),
      $mime,
      '-',
      t('Failed'),
    ), 'class' => 'ffw2-formats-failed');
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No formats.'), 'colspan' => 4));
  }

  return theme('table', $header, $rows, array('class' => 'ffw2-formats'));
}

//////////////////////////////////////////////////////////////////////////////
// File previews

/**
 * Outputs an embeddable XHTML snippet containing a viewer for a file, and
 * terminates the page request.
 *
 * This is suitable for use in Ajax-based file previews, for instance.
 *
 * @param $node
 *   A node object as returned from node_load().
 * @param $handler
 *   A handler to display object.
 */
function ffw2_embed($node, $handler) {
  if ($output = ffw2_wrapper_html($node, $handler, array('max_width' => 0, 'max_height' => 0))) {
    print $output;
  }
  else {
    drupal_set_header('HTTP/1.0 404 Not Found');
    drupal_set_header('Content-Type: text/plain; charset=utf-8');
    print '404 Not Found';
  }

  exit();
}

//////////////////////////////////////////////////////////////////////////////
// File serve

/**
 * Redirects to the file view or download path.
 *
 * @param $node
 *   A node object as returned from node_load().
 * @param $vid
 *   A node version ID.
 * @param $disposition
 *   Disposition of the file. Might be 'download' or 'inline'.
 */
function ffw2_serve($node, $vid, $disposition = 'attachment') {
  if (isset($vid) && $node->vid != $vid) {
    $node = node_load($node->nid, $vid);
  }
  if ($node->type != 'ffw2' || !is_object($node->ffw2) || (!empty($vid) && !is_numeric($vid))) {
    drupal_not_found();
    exit;
  }
  $href = bitcache_resolve_uri($node->ffw2->uri);
  $query = array('vid' => isset($vid) ? $vid : $node->vid, 'disposition' => $disposition, 'op' => ($disposition == 'attachment') ? 'download' : 'view');
  drupal_goto($href, $query);
}

//////////////////////////////////////////////////////////////////////////////
// File metadata

/**
 * Prints file metadata.
 *
 * @param $uri
 *   A blob uri.
 * @param $nid
 *   A node ID.
 *
 * @return
 *   Metadata html page
 */
function ffw2_metadata($uri, $nid) {

  $access = FALSE;
  $uri = bitcache_uri($uri);
  $parent_uri = rdf_value($uri, rdf_qname_to_uri('dc:source'));

  $files = array_merge(
    isset($_SESSION['ffw2_preview_file']) && is_object($_SESSION['ffw2_preview_file']) ? array($_SESSION['ffw2_preview_file']) : array(),
    isset($_SESSION['ffw2_attach_files']) && is_array($_SESSION['ffw2_attach_files']) ? $_SESSION['ffw2_attach_files'] : array(),
    isset($_SESSION['ffw2_cck_files']) && is_array($_SESSION['ffw2_cck_files']) ? $_SESSION['ffw2_cck_files'] : array()
  );

  foreach ($files as $file) {
    if (is_object($file) && $file->uri == $parent_uri || $file->uri == $uri) {
      $access = TRUE;
    }
  }

  if (is_numeric($nid)) {
    $node = node_load($nid);
    if (node_access('view', $node) && $node->type == 'ffw2' && db_result(db_query("SELECT f.* FROM {ffw2_nodes} f WHERE f.nid = %d AND (f.uri = '%s' OR f.uri = '%s')", $node->nid, $uri, $parent_uri))) {
      $access = TRUE;
    }
  }

  if ($access && $rdf = rdf_normalize(rdf_query($uri, NULL, NULL, array('repository' => FFW2_RDF_REPOSITORY)))) {
    $namespaces = rdf_get_namespaces();
    $info = ffw2_get_metadata_info();
    foreach ($rdf as $subject => $predicates) {
      foreach ($predicates as $predicate => $objects) {
        foreach ($objects as $object) {
          $rdf = rdf_uri_to_qname($predicate, $namespaces, FALSE);
          if (isset($info[$rdf]) && $name = $info[$rdf]['name']) {
            $data[] = array($name, isset($info[$rdf]['theme']) ? theme($info[$rdf]['theme'], is_object($object) ? $object->value : $object) : theme('rdf_value', $object));
          }
        }
      }
    }
    return theme('ffw2_metadata', $data);
  }

  drupal_access_denied();
  exit;
}

//////////////////////////////////////////////////////////////////////////////
// OG integration

/**
 * Prints og file browser.
 *
 * @param $node
 *   A group node object.
 *
 * @return
 *   Browser html page
 */
function ffw2_og_browser($node, $vid) {
  drupal_set_title(check_plain($node->title));
  if (isset($node->og_vocabularies) && is_array($node->og_vocabularies) && count($vids = array_intersect(array_keys($node->og_vocabularies), array_keys(taxonomy_get_vocabularies('ffw2')))) > 0) {
    if (isset($vid) && in_array($vid, $vids)) {
      return ffw2_browser_page($vid);
    }
    else {
      $script = (count($vids) == 1) ? '<script type=\'text/javascript\'>$(document).ready(function() { $("#ffw2-folder-v'. reset($vids) .'-g'. $node->nid .'-bpage > div.ffw2-cells").click(); });</script>' : '';
      return ffw2_browser_page($vids, NULL, $script);
    }
  }
  else {
    return t('No group vocabularies defined.');
  }
}

/**
 * Prints og file gallery.
 *
 * @param $node
 *   A group node object.
 *
 * @return
 *   Browser html page
 */
function ffw2_og_gallery($node, $vid, $tid, $filter) {
  drupal_set_title(check_plain($node->title));
  if (isset($node->og_vocabularies) && is_array($node->og_vocabularies) && count($vids = array_intersect(array_keys($node->og_vocabularies), array_keys(taxonomy_get_vocabularies('ffw2')))) > 0) {
    if (isset($vid) && in_array($vid, $vids)) {
      return ffw2_gallery_page($vid, $tid, $filter, 'ffw2');
    }
    else {
      return (count($vids) == 1) ? ffw2_gallery_page(reset($vids), NULL, NULL, 'ffw2') : ffw2_gallery_page($vids, NULL, NULL, 'ffw2');
    }
  }
  else {
    return t('No group vocabularies defined.');
  }
}

/**
 * Prints file upload progress.
 *
 * @param $id
 *   Upload ID.
 *
 * @return
 */
function ffw2_progress($id, $module) {
  if (FFW2_APC && $id) {
    $status = apc_fetch(FFW2_APC_PREFIX . $id);
    $percentage = (is_array($status) && isset($status['total']) && $status['total'] != 0) ? ceil($status['current']/$status['total']*100) : -1;
    $progress = module_invoke_all('ffw2_progress', $module, $id, $percentage) + array('status' => TRUE, 'percentage' => $percentage, 'message' => $percentage < 100 ? t('Uploading...') : t('Processing...'));
    drupal_json($progress);
  }
  exit;
}

