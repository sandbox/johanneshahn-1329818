
/**
 * Conditionally show or hide settings.
 */
Drupal.behaviors.ffw2_admin = function () {
  $('div.ffw2-maintenance-ffw2-cron-setting input.form-radio').click(function () {
    if (this.value == '1') {
      $('div.ffw2-maintenance-ffw2-cron-limit-size-setting').show();
      $('div.ffw2-maintenance-ffw2-cron-limit-total-setting').show();
    }
    else {
      $('div.ffw2-maintenance-ffw2-cron-limit-size-setting').hide();
      $('div.ffw2-maintenance-ffw2-cron-limit-total-setting').hide();
    }
  });
  $('div.ffw2-autodetection-ffw2-mime-autodetection-setting input.form-radio').click(function () {
    if (this.value == '2') {
      $('div.ffw2-autodetection-ffw2-mime-autodetection-conditions-setting').show();
    }
    else {
      $('div.ffw2-autodetection-ffw2-mime-autodetection-conditions-setting').hide();
    }
  });
};

