
/**
 * Conditionally show or hide settings.
 */
Drupal.behaviors.ffw2_image_admin = function () {
  $('div.ffw2-image-exif-ffw2-image-exif-setting input.form-checkbox').click(function () {
    if (this.checked) {
      $('div.ffw2-image-exif-ffw2-image-exif-data-setting').show();
      $('div.ffw2-image-geo-ffw2-image-geo-setting input.form-checkbox').attr('disabled', '');
    }
    else {
      $('div.ffw2-image-exif-ffw2-image-exif-data-setting').hide();
      $('div.ffw2-image-geo-ffw2-image-geo-setting input.form-checkbox').attr('disabled', 'disabled');
    }
  });
};

