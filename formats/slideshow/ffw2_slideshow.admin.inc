<?php

/**
 * @file
 * Module admin page callbacks.
 */

//////////////////////////////////////////////////////////////////////////////
// File slideshow settings

/**
 * Implements the settings page.
 *
 * @return
 *   The form structure.
 */
function ffw2_slideshow_admin_settings() {
  drupal_add_js(drupal_get_path('module', 'ffw2_slideshow') .'/ffw2_slideshow_admin.js');
  $form = array();

  // PDF settings.
  $form['pdf'] = array('#type' => 'fieldset', '#title' => t('PDF settings'), '#description' => _ffw2_command_exists('pdfinfo') ? t('"pdfinfo" utility is installed in the system.') : t('"pdfinfo" utility is not installed in the system.'), '#collapsible' => TRUE, '#collapsed' => FALSE);
  $form['pdf']['ffw2_slideshow_pdf'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable Extraction of the PDF metadata'),
    '#default_value' => _ffw2_command_exists('pdfinfo') ? FFW2_SLIDESHOW_PDF : 0,
    '#description'   => t('Check this box if you want to extract any of the following metadata.'),
    '#disabled'      => _ffw2_command_exists('pdfinfo') ? FALSE : TRUE,
    '#prefix' => '<div class="ffw2-slideshow-pdf-ffw2-slideshow-pdf-setting">',
    '#suffix' => '</div>',
  );
  $pdf = _ffw2_slideshow_pdf();
  foreach ($pdf as $name => $data) {
    $pdf_data_default[$name] = $data['default'];
  }
  $pdf_data = variable_get('ffw2_slideshow_pdf_data', $pdf_data_default);
  $i = $j = 0;
  foreach ($pdf as $name => $data) {
    $form['pdf']['pdf_'. $i][$j]['ffw2_slideshow_pdf_'. $name] = array(
      '#type'          => 'checkbox',
      '#title'         => $data['name'],
      '#default_value' => $pdf_data[$name],
    );
    $j++;
    if ($j == 3) {
      $i++;
      $j = 0;
    }
  }

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['reset'] = array(
    '#type'  => 'submit',
    '#value' => t('Reset to defaults'),
  );

  return $form;
}

/**
 * Validate hook for the settings form.
 */
function ffw2_slideshow_admin_settings_validate($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  switch ($op) {
    case t('Save configuration'):
      break;
  }
}

/**
 * Submit hook for the settings form.
 */
function ffw2_slideshow_admin_settings_submit($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  $values = $form_state['values'];
  switch ($op) {
    case t('Save configuration'):
      variable_set('ffw2_slideshow_pdf', $values['ffw2_slideshow_pdf']);
      $pdf = _ffw2_slideshow_pdf();
      $pdf_data = array();
      foreach ($pdf as $name => $data) {
        $pdf_data[$name] = $values['ffw2_slideshow_pdf_'. $name];
      }
      variable_set('ffw2_slideshow_pdf_data', $pdf_data);
      drupal_set_message(t('The configuration options have been saved.'));
      break;
    case t('Reset to defaults'):
      variable_del('ffw2_slideshow_pdf');
      variable_del('ffw2_slideshow_pdf_data');
      drupal_set_message(t('The configuration options have been reset to their default values.'));
      break;
  }
}

