<?php

/**
 * @file
 * Themes for slideshow file formats.
 */

//////////////////////////////////////////////////////////////////////////////
// Theme callbacks

/**
 * Theme for admin PDF checkboxes.
 */
function theme_ffw2_slideshow_admin_settings($form) {
  $rows = array();
  foreach ($form['pdf'] as $name => $element) {
    if (preg_match('/pdf_/', $name)) {
      $rows[] = array(
        drupal_render($form['pdf'][$name][0]),
        drupal_render($form['pdf'][$name][1]),
        drupal_render($form['pdf'][$name][2])
      );
      unset($form['pdf'][$name]);
    }
  }
  $form['pdf']['pdf'] = array(
    '#type' => 'markup',
    '#value' => theme('table', NULL, $rows),
    '#prefix' => '<div class="ffw2-slideshow-pdf-ffw2-slideshow-pdf-data-setting'. (_ffw2_command_exists('pdfinfo') && FFW2_SLIDESHOW_PDF ? '' : ' js-hide') .'">',
    '#suffix' => '</div>',
    );
  return drupal_render($form);
}

