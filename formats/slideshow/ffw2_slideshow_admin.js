
/**
 * Conditionally show or hide settings.
 */
Drupal.behaviors.ffw2_slideshow_admin = function () {
  $('div.ffw2-slideshow-pdf-ffw2-slideshow-pdf-setting input.form-checkbox').click(function () {
    if (this.checked) {
      $('div.ffw2-slideshow-pdf-ffw2-slideshow-pdf-data-setting').show();
    }
    else {
      $('div.ffw2-slideshow-pdf-ffw2-slideshow-pdf-data-setting').hide();
    }
  });
};

