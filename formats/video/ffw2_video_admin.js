
/**
 * Conditionally show or hide settings.
 */
Drupal.behaviors.ffw2_video_admin = function () {
  $('div.ffw2-video-getid3-ffw2-video-getid3-setting input.form-checkbox').click(function () {
    if (this.checked) {
      $('div.ffw2-video-getid3-ffw2-video-getid3-data-setting').show();
    }
    else {
      $('div.ffw2-video-getid3-ffw2-video-getid3-data-setting').hide();
    }
  });
};

